package Controlador.DAO;

import Modelo.*;
import Modelo.Repositorio.Persistencia;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class AlquilerDAO {
    private  static EntityManager em = Persistencia.getInstancia().getEm();

    public static List<Alquiler> listarAlquiler(){
        Query consulta = em.createNamedQuery("alquiler.listar");
        return  consulta.getResultList();
    }

    //metodo de busqueda
    public static List<Alquiler> buscarCodigo(String filtro){
        Query consulta = em.createNamedQuery("alquiler.buscarCliente");
        consulta.setParameter("filtro", filtro);
        return consulta.getResultList();
    }

    public static void crear(Alquiler nuevoAlquiler) {
        em.getTransaction().begin();
        em.persist(nuevoAlquiler);
        em.getTransaction().commit();

        Cliente cliente = nuevoAlquiler.getPerteneceCliente();
        cliente.getTieneAlquiler().add(nuevoAlquiler);

        Bicicleta bicicleta = nuevoAlquiler.getTieneBicicleta();
        bicicleta.getPerteneceAlquiler().add(nuevoAlquiler);

        Trabajador trabajador = nuevoAlquiler.getPerteneceTrabajador();
        trabajador.getRealizaAlquiler().add(nuevoAlquiler);
    }
}
